#! /usr/bin/env python3
import requests
import sys

def currentWeather():
    try:
        api_call = requests.get(
            "https://r19kigmlg8.execute-api.us-east-1.amazonaws.com/default/jmillette-current-weather"
        )
        results = api_call.json()
        print(results)
        api_call.raise_for_status()
    except requests.exceptions.HTTPError as e:
        print(e)
        return None


def timed_Weather(time):
    try:
        api_call = requests.get(
            f"https://r19kigmlg8.execute-api.us-east-1.amazonaws.com/default/jmillette-timed-weather?time={time}"
        )
        results = api_call.json()
        print(results)
        api_call.raise_for_status()
    except requests.exceptions.HTTPError as e:
        print(e)
        return None


if __name__ == "__main__":
    if len(sys.argv) > 1:
        timed_Weather(sys.argv[1])
    else:
        currentWeather()
        

